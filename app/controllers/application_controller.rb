class ApplicationController < ActionController::Base
  before_filter :authorize
  include Clearance::Controller
  protect_from_forgery
end
