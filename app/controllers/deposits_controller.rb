class DepositsController < ApplicationController
  def new
    @deposit = Deposit.new
  end
  def is_number?(object)
  true if Integer(object) rescue false
  end
  def create
        @deposit = Deposit.new
        if @deposit.init(deposit_params[:amount].to_i,current_user.id)
          redirect_to root_url, alert: "Amount Deposited"
        else
          flash[:notice] = "Amount Entered shall be a positive Integer"
          redirect_to :action => "new"
        end
  end
  private
  def deposit_params
    params.require(:deposit).permit(:amount)
  end

end
