class TransfersController < ApplicationController
  def new
    @transfer = Transfer.new
  end
  def is_number?(object)
  true if Integer(object) rescue false
  end
  def create
      @sender=User.find(current_user.id)
      if @sender.deficit(transfer_params[:amount].to_i)
        flash[:notice] = "The amount you entered is more than what you have, you will run into a deficit"
        redirect_to :action => "new"
      else
        @transfer = Transfer.new
        if  @transfer.init(transfer_params[:amount].to_i,current_user.id,transfer_params[:receiver_id])
            redirect_to root_url, alert: "Amount Transfered"
        else        
          flash[:notice] = "Amount Entered shall be a positive Integer"
          redirect_to :action => "new"

        end 
      end

  end
  private
  def transfer_params
    params.require(:transfer).permit(:amount, :receiver_id)
  end
end
