class WithdrawsController < ApplicationController
  def new
    @withdraw = Withdraw.new
  end
  def is_number?(object)
  true if Integer(object) rescue false
  end
  def create
      @user=User.find(current_user.id)
      if @user.deficit(withdraw_params[:amount].to_i)
          flash[:notice] = "The amount you entered is more than what you have, you will run into a deficit"
          redirect_to :action => "new"

      else
        @withdraw = Withdraw.new
        if !@withdraw.init(withdraw_params[:amount].to_i,current_user.id)
          flash[:notice] = "Amount Entered shall be a positive integer"
          redirect_to :action => "new"
        else
          redirect_to root_url, alert: "Amount Withdrawn"
        end         
      end
  end
  private
  def withdraw_params
    params.require(:withdraw).permit(:amount)
  end

end
