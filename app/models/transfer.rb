class Transfer < ActiveRecord::Base
  attr_accessible :amount, :receiver, :sender
  validates :amount, :numericality => { :greater_than => 0 }
  belongs_to :sender, :class_name => 'User', :foreign_key => 'sender_id'
  belongs_to :receiver, :class_name => 'User', :foreign_key => 'receiver_id'
  def init(amount, sender_id,receiver_id)
    @sender=User.find(sender_id)
    @receiver=User.find(receiver_id)
      transaction do
        self.amount=amount
        self.sender_id=sender_id
        self.receiver_id=receiver_id
        if !self.save
          return false
        end
        @receiver.deposit(amount)
        @sender.withdraw(amount)
      end
      return true
   
  
  end
end
