class User < ActiveRecord::Base
  include Clearance::User
  has_many :sent_transfers, :class_name => 'Transfer', :foreign_key => 'sender_id', :dependent => :destroy
  has_many :received_transfers, :class_name => 'Transfer', :foreign_key => 'receiver_id', :dependent => :destroy
  has_many :deposits, :class_name => 'Deposit', :foreign_key => 'user_id', :dependent => :destroy
  has_many :withdraws, :class_name => 'Withdraw', :foreign_key => 'user_id', :dependent => :destroy
  before_save :default_values
  def default_values
    self.balance ||= 0
  end
  def deposit(amount)
    self.balance=self.balance+amount
    self.save
  end
  
  def withdraw(amount)
    self.balance=self.balance-amount
    self.save
  end
  def deficit(amount)
    if self.balance<amount
      return true
    else
      return false
    end
  end
end
