class Withdraw < ActiveRecord::Base
  attr_accessible :amount, :user_id
  validates :amount, :numericality => { :greater_than => 0 }
  belongs_to :sender, :class_name => 'User', :foreign_key => 'user_id'
  def init(amount, user_id)
        @user=User.find(user_id)
          transaction do
              self.amount=amount
              self.user_id=user_id
              @user.withdraw(amount)
              if !self.save
                return false
              end
          end
        return true
            
  end

end
