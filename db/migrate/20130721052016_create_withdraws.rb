class CreateWithdraws < ActiveRecord::Migration
  def change
    create_table :withdraws do |t|
      t.integer :amount
      t.integer :user_id

      t.timestamps
    end
  end
end
